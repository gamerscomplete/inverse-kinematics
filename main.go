package main

import (
    "fmt"
    "math"
)

const (
    // Leg segment lengths (in millimeters)
    coxaLength   = 50
    femurLength  = 70
    tibiaLength  = 120

    // Leg joint angles (in degrees)
    coxaAngle    = 60
    femurAngle   = 60
    tibiaAngle   = 60

    // Leg joint limits (in degrees)
    coxaMin      = -120
    coxaMax      = 120
    femurMin     = -180
    femurMax     = 180
    tibiaMin     = -120
    tibiaMax     = 120

    // Robot body dimensions (in millimeters)
    bodyWidth    = 200
    bodyLength   = 300
    bodyHeight   = 100
)

// Leg struct represents a single leg of the hexapod
type Leg struct {
    coxa, femur, tibia float64 // Joint angles (in degrees)
}

// Hexapod struct represents the entire hexapod robot
type Hexapod struct {
    legs [6]Leg // Array of legs
}

func main() {
    // Create a hexapod with default joint angles
    hexapod := NewHexapod([6][3]float64{
        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0},
    })

    // Move the front right leg to a new position
//    x, y, z := 100.0, 100.0, 0.0
	x, y, z := 120.0, 50.0, -80.0
    coxa, femur, tibia, ok := hexapod.legs[0].InverseKinematics(x, y, z)
    if ok {
        fmt.Printf("New joint angles for leg 0: coxa=%.2f, femur=%.2f, tibia=%.2f\n", coxa, femur, tibia)
    } else {
        fmt.Println("No solution found for leg 0")
    }
}


// NewHexapod creates a new hexapod with the given leg joint angles
func NewHexapod(legAngles [6][3]float64) *Hexapod {
    hexapod := &Hexapod{}

    // Initialize each leg with the given joint angles
    for i := 0; i < 6; i++ {
        hexapod.legs[i] = Leg{legAngles[i][0], legAngles[i][1], legAngles[i][2]}
    }

    return hexapod
}

// ForwardKinematics calculates the position of the end effector (foot) of a single leg
// based on the current joint angles and the leg geometry.
func (leg *Leg) ForwardKinematics() (x, y, z float64) {
    // Convert joint angles to radians
    coxaRad := degToRad(leg.coxa + coxaAngle)
    femurRad := degToRad(leg.femur + femurAngle)
    tibiaRad := degToRad(leg.tibia + tibiaAngle)

    // Calculate the position of the end effector (foot) of the leg
    x = coxaLength*math.Cos(coxaRad) + femurLength*math.Cos(coxaRad+femurRad) + tibiaLength*math.Cos(coxaRad+femurRad+tibiaRad)
    y = coxaLength*math.Sin(coxaRad) + femurLength*math.Sin(coxaRad+femurRad) + tibiaLength*math.Sin(coxaRad+femurRad+tibiaRad)
    z = 0.0 // Assume leg is attached to the ground

    return
}

// InverseKinematics calculates the joint angles required to move the end effector (foot)
// of a single leg to the specified position and orientation.
func (leg *Leg) InverseKinematics(x, y, z float64) (coxa, femur, tibia float64, ok bool) {
    // Solve for the coxa angle
    coxa = radToDeg(math.Atan2(y, x))
    if coxa < coxaMin || coxa > coxaMax {
        return 0, 0, 0, false
    }

    // Calculate the distance from the base of the leg to the end effector
    d := math.Sqrt(x*x + y*y)

    // Calculate the position of the end effector projected onto the x-y plane
    x0 := d - coxaLength
    z0 := z

    // Calculate the length of the projection of the leg onto the x-y plane
    l := math.Sqrt(x0*x0 + z0*z0)

    // Solve for the femur and tibia angles
    cosTibia := (femurLength*femurLength + tibiaLength*tibiaLength - l*l) / (2 * femurLength * tibiaLength)
    tibiaRad := math.Acos(cosTibia)
    femurRad := math.Atan2(z0, x0) - math.Atan2(tibiaLength*math.Sin(tibiaRad), femurLength+tibiaLength*cosTibia)
    femur = radToDeg(femurRad) - femurAngle
    tibia = radToDeg(tibiaRad) - tibiaAngle

    // Check if the solution is valid
    if femur < femurMin || femur > femurMax || tibia < tibiaMin || tibia > tibiaMax {
		fmt.Println("Femur:", femur, " Tibia: ", tibia)
        return 0, 0, 0, false
    }

    return coxa - coxaAngle, femur, tibia, true
}

// degToRad converts an angle in degrees to radians.
func degToRad(deg float64) float64 {
    return deg * (math.Pi / 180)
}

// radToDeg converts an angle in radians to degrees.
func radToDeg(rad float64) float64 {
    return rad * (180 / math.Pi)
}
